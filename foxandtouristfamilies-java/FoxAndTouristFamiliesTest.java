import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FoxAndTouristFamiliesTest {

    protected FoxAndTouristFamilies solution;

    @Before
    public void setUp() {
        solution = new FoxAndTouristFamilies();
    }

    public static void assertEquals(double expected, double actual) {
        if (Double.isNaN(expected)) {
            Assert.assertTrue("expected: <NaN> but was: <" + actual + ">", Double.isNaN(actual));
            return;
        }
        double delta = Math.max(1e-9, 1e-9 * Math.abs(expected));
        Assert.assertEquals(expected, actual, delta);
    }

    @Test
    public void testCase0() {
        int[] A = new int[]{0, 1};
        int[] B = new int[]{1, 2};
        int[] f = new int[]{0};

        double expected = 1.5;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        int[] A = new int[]{0, 1};
        int[] B = new int[]{1, 2};
        int[] f = new int[]{0, 0};

        double expected = 1.25;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        int[] A = new int[]{0, 1};
        int[] B = new int[]{1, 2};
        int[] f = new int[]{0, 1};

        double expected = 0.75;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        int[] A = new int[]{0, 1};
        int[] B = new int[]{1, 2};
        int[] f = new int[]{0, 2};

        double expected = 1.0;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        int[] A = new int[]{0, 0, 0};
        int[] B = new int[]{1, 2, 3};
        int[] f = new int[]{0};

        double expected = 1.0;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase5() {
        int[] A = new int[]{0, 0, 0};
        int[] B = new int[]{1, 2, 3};
        int[] f = new int[]{1, 2};

        double expected = 0.7777777777777777;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase6() {
        int[] A = new int[]{0, 1, 1, 3, 5, 6};
        int[] B = new int[]{1, 2, 3, 4, 4, 4};
        int[] f = new int[]{5, 6, 1};

        double expected = 0.4537037037037037;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

    @Test
    public void testCase7() {
        int[] A = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] B = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] f = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        double expected = 1.4914341925000003;
        double actual = solution.expectedLength(A, B, f);

        assertEquals(expected, actual);
    }

}
