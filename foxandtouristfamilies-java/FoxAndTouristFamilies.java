import java.util.Arrays;

public class FoxAndTouristFamilies {

	public double expectedLength(int[] A, int[] B, int[] f) {
		int[] cityPop = new int[A.length + 1];
		for (int i : f) {
			cityPop[i]++;
		}

		boolean[] visited = new boolean[A.length];
		double[] numTravels = new double[A.length];

		for (int i = 0; i < cityPop.length; i++) {
			if (cityPop[i] > 0) {
				Arrays.fill(visited, false);
				search(A, B, numTravels, visited, cityPop[i], i, true);
			}
		}

		double sum = 0.0;
		for (int i = 0; i < numTravels.length; i++) {
			sum += numTravels[i];
		}
		return sum / (A.length * f.length);
	}

	int search(int[] A, int[] B, double[] numTravels, boolean[] visited,
			int pop, int currCity, boolean home) {
		int cities = 1;
		for (int i = 0; i < A.length; i++) {
			if (visited[i]) {
				continue;
			}
			if (A[i] == currCity || B[i] == currCity) {
				visited[i] = true;
				int branchCities = search(A, B, numTravels, visited, pop,
						A[i] == currCity ? B[i] : A[i], false);
				numTravels[i] += pop * Math.pow(branchCities, pop);
				cities += branchCities;
			}
		}
		return cities;
	}
}
