import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ICPCBalloons {

	public int minRepaintings(int[] balloonCount, String balloonSize,
			int[] maxAccepted) {
		List<Integer> bMedium = new ArrayList<Integer>();
		List<Integer> bLarge = new ArrayList<Integer>();
		for (int i = 0; i < balloonCount.length; i++) {
			(balloonSize.charAt(i) == 'M' ? bMedium : bLarge)
					.add(balloonCount[i]);
		}
		Collections.sort(bMedium);
		Collections.sort(bLarge);
		Arrays.sort(maxAccepted);

		return minRepaintings(bMedium, bLarge, new ArrayList<Integer>(),
				new ArrayList<Integer>(), maxAccepted, 0);
	}

	int minRepaintings(List<Integer> bMedium, List<Integer> bLarge,
			List<Integer> pMedium, List<Integer> pLarge, int[] pAll, int k) {

		if (k == pAll.length) {
			int med = calcMinRepaints(bMedium, pMedium);
			int lrg = calcMinRepaints(bLarge, pLarge);
			return med == -1 || lrg == -1 ? -1 : med + lrg;
		}

		pMedium.add(pAll[k]);
		int min = minRepaintings(bMedium, bLarge, pMedium, pLarge, pAll, k + 1);
		pMedium.remove(pMedium.size() - 1);

		pLarge.add(pAll[k]);
		int min2 = minRepaintings(bMedium, bLarge, pMedium, pLarge, pAll, k + 1);
		pLarge.remove(pLarge.size() - 1);

		if (min == -1) {
			return min2;
		} else if (min2 == -1) {
			return min;
		}
		return min < min2 ? min : min2;
	}

	int calcMinRepaints(List<Integer> balloons, List<Integer> probs) {
		int missingBalloons = 0;
		int leftBalloons = 0;

		for (int i = 0; i < balloons.size() && i < probs.size(); i++) {
			int b = balloons.get(balloons.size() - i - 1);
			int p = probs.get(probs.size() - i - 1);
			if (b < p) {
				missingBalloons += p - b;
			} else if (p < b) {
				leftBalloons += b - p;
			}
		}
		if (balloons.size() > probs.size()) {
			for (int i = 0; i < balloons.size() - probs.size(); i++) {
				leftBalloons += balloons.get(i);
			}
		} else {
			for (int i = 0; i < probs.size() - balloons.size(); i++) {
				missingBalloons += probs.get(i);
			}
		}
		return missingBalloons > leftBalloons ? -1 : missingBalloons;
	}
}
