import java.util.Arrays;

public class FoxAndVacation {

	public int maxCities(int total, int[] d) {
		Arrays.sort(d);
		int currDays = 0;
		int i;
		for(i = 0; i < d.length; i++) {
			if(currDays + d[i] > total) {
				break;
			}
			currDays += d[i];
		}
		return i;
	}
}
